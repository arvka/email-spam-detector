﻿
# EMAIL SPAM DETECTOR

This is an application email spam detector for analyzing email in a file text. This application using naive bayes method for analyzing whether an email is detected as spam or not. This application use SPAMASSASINS dataset as data training. This application developed using Java language with help of netbeans IDE.

## GETTING STARTED

Download it as zip or clone this, then you can edit the source code in Netbeans IDE or else. Please think it yourself how to run this source code.

### Important Note

Before running the program, copy and paste trainData and TEST DATA folder in different location. Then change line 28-29 in proses.java file based on trainData folder location into :
```
	String path_data_training_spam = "[trainData folder location]\\spam";
    String path_data_training_ham = "[trainData folder location]\\ham";
```

Example : 
```
	String path_data_training_spam = "C:\\Users\\HP\\Desktop\\Pengenalan Pola\\datasetSpam\\cleanedDataset\\spam";
    String path_data_training_ham = "C:\\Users\\HP\\Desktop\\Pengenalan Pola\\datasetSpam\\cleanedDataset\\ham";
```

## Folder Information

```
Project   
│
└───TEST DATA // This folder containing data test for testing this application
│   │   ham.11311a8e5dbfe18503bf736b82b91fc7
│   │   ....
│   │	spam.cec5f611ae665ff0add6c4928d47f2be
│   
└───build
│   
└───dist
│   
└───nbproject
│   
└───src
│   │
│   └───CobaManual
│   
└───trainData // This folder containing email data training
│   │
│   └───ham // Data training for ham email (identified as not a spam)
│   │
│   └───spam // Data training for ham email (identified as spam)
│   
└───build.xml
│   
└───manifest.mf
```

## Built With

* [Netbeans IDE](https://netbeans.org/downloads) - IDE used for code writing
* Java Language
* [SPAMASSASINS Emai Dataset](http://csmining.org/index.php/spam-assassin-datasets.html) - Data training 
* Brain - Used for thinking
* Mouth - Used for swearing in javanese if errors occurr
* Hand - For typing and do useless thing for relaxation

## Our Team

* **Arka Fadila Yasa** - *Initial work, Project Manager, and Main Programmer* - [arvka](https://gitlab.com/arvka)
* **Akhmad Rohim** - *Secondary Programmer, Method Manualization*
* **Frisma Yessy Nabella** - *Documentation*
* **Rakha Fakhruddin** - *Feature Acquisition and Extraction*


## License

See in license page and of course each member of this team have driving license.

## Thanks to 

 - Allah, our almighty god. So we can complete this project.
 - Our parents.
 - Our pattern recognition course lecture.
 - My lov... eh wait, i'm still single.
 - Random person who read this readme file, congratulations !! yout are the one of the rarest person in the Round Earth.

