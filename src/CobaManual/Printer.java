/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CobaManual;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author HP
 */
public class Printer {

//    Deklarasi dan instantsiasi kelas StringBuilder
    public static StringBuilder sb = new StringBuilder();
    //Inisialiasai lokasi file yang ingin disimpan
    private String location = "C:\\"+"hasil.csv";

    //Method yang berfunsi untuk menambahkan karakter String untuk variabel sb
    public void appendString(String s) {
        sb.append(s);
    }

    /*Method untuk menentukan lokasi dari file yang akan disimpan, Masukan method berisi nilai
    String lokasi file dan nama file yang ingin disimpan dalam bentuk csv */
    public void setLocation(String loc) {
        if(loc.endsWith(".csv")){
            this.location = loc;
        }else{
            this.location = loc+".csv";
        }
        
    }

    /*Method yang berfungsi untuk menulis isi file yang berasal dari nilai variabel sb kemudian
      menyimpannya pada lokasi yang telah ditentukan*/
    public void printCSV() {
        File file = new File(location);
        try (FileOutputStream fop = new FileOutputStream(file)) {
            if (!file.exists()) {
                file.createNewFile();
            }
            byte[] contentInBytes = sb.toString().getBytes();
            fop.write(contentInBytes);
            fop.flush();
            fop.close();
            System.out.println("Done");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /*Method untuk mencetak nilai String dari isi nilai file yang akan di simpan
    ke konsol aplikasi*/
    public void printConsole(){
        System.out.println(sb.toString());
    }
    
    // method untuk menghapus semua nilai variabel sb
    public void erase(){
        sb.delete(0, sb.capacity()-1);
    }
    //Method untuk mendapatkan total karakter yang tersimpan pada variabel sb
    public int getSize(){
        return sb.capacity();
    }
}
