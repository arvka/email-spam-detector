/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CobaManual;

import java.io.File;

/**
 *
 * @author ochim
 */
public class proses {
//    Deklarasi variabel untuk menampung kelas data
    kelas spam,ham;
//    Deklarasi variabel untuk menampung nilai perhitungan naive bayes
    double prior_spam, prior_ham;
    double posterior_spam = 1, posterior_ham = 1;
    double evidance;
    double total_likelihood_spam, total_likelihood_ham;
//    Deklarasi dan instansiasi dari kelas Printer
    Printer pr = new Printer();
//    Deklarasi dan instansiasi dari kelas StringBuilder
    StringBuilder sbp, tmpMulti = new StringBuilder(), buildDisplay;
//    Menentukan file path dari masing-masing kelas data
    // NOTE : alamat folder dibawah merupakan alamat data train di pc author, ganti dengan alamat folder dataTrain yang ada di komputer anda
    String path_data_training_spam = "C:\\Users\\HP\\Desktop\\Pengenalan Pola\\datasetSpam\\cleanedDataset\\spam";
    String path_data_training_ham = "C:\\Users\\HP\\Desktop\\Pengenalan Pola\\datasetSpam\\cleanedDataset\\ham";
//    Deklarasi dan inisialisasi variabell yang akan dipakai perhitungan akurasi
    int spamAwal = 0,hamAwal = 0,hasilSpam = 0,hasilHam = 0,salahSpam = 0,salahHam = 0;
    
/*    Fungsi untuk mencari klasifikasi file yang berisi email menggunakan metode naive bayes
      Masukan fungsi berupa alamat file berada kemudian fungsi melakukan perhitungan pada satu file */
    public void prosesFile(String path_data_cari) {
        /*Menghapus nilai pada variabel jika telah dipakai pada perhitungan sebelumnya dan nilai variabel
        tidak diisi */
        if (pr.getSize() > 0) {
            pr.erase();
        }
//        Proses pemecahan Sting kemudian dilakukan klasifikasi 
        fitur f = new fitur();
        System.out.print("| Dokumen ");
        pr.appendString(";dokumen");
        for (int i = 0; i < fitur.keepAttribute.length; i++) {
            System.out.printf("| %10s ", fitur.keepAttribute[i]);
            pr.appendString(";" + fitur.keepAttribute[i]);
        }
        System.out.print("|\n");
        pr.appendString(";\n");
        spam = new kelas(path_data_training_spam, "spam");
        ham = new kelas(path_data_training_ham, "ham");
        System.out.printf("| %7s ", "email");
        pr.appendString(";email");
        f.isi_nilai(spam.bacaFile(path_data_cari));
        this.set_prior();
        this.set_posterior_evidance(f.nilai_fitur);
    }

//    Method ini dipanggil jika user telah menekan tombol proses multifile
    public void prosesFolder(String folderPath) {
        if (pr.getSize() > 0) {
            pr.erase();
        }
        sbp = new StringBuilder();
        System.out.print("| Dokumen ");
        pr.appendString(";dokumen");
        for (int i = 0; i < fitur.keepAttribute.length; i++) {
            System.out.printf("| %10s ", fitur.keepAttribute[i]);
            pr.appendString(";" + fitur.keepAttribute[i]);
        }
        System.out.print("|\n");
        pr.appendString(";\n");
        spam = new kelas(path_data_training_spam, "spam");
        ham = new kelas(path_data_training_ham, "ham");
        File folder = new File(folderPath);
        File[] listOfFiles = folder.listFiles();
        int jumlah = folder.listFiles().length;
        pr.appendString("\n");
        buildDisplay = new StringBuilder();
        
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                System.out.printf("| %7s ", "email");
                pr.appendString(";email");
                fitur f = new fitur();
                f.isi_nilai(spam.bacaFile(listOfFiles[i].getAbsolutePath()));
                this.set_prior();
                if(f.kelas.equalsIgnoreCase("spam")){
                    spamAwal += 1;
                }else if(f.kelas.equalsIgnoreCase("ham")){
                    hamAwal += 1;
                }
                this.set_posterior_evidance(f.nilai_fitur);
                buildDisplay(f.kelas);
            }
        }
        //Proses menyusun strng untuk ditampilkan pada textArea 
        pr.appendString(tmpMulti.toString());
        buildDisplay.append("\nJumlah spam diketahui = " + spamAwal);
        buildDisplay.append("\nJumlah ham diketahui = " + hamAwal);
        buildDisplay.append("\nJumlah diklasifikasikan spam = " + hasilSpam);
        buildDisplay.append("\nJumlah diklasifikasikan ham = " + hasilHam);
        buildDisplay.append("\nJumlah salah deteksi spam = " + salahSpam);
        buildDisplay.append("\nJumlah salah deteksi ham = " + salahHam);
        int totalAkurat = Math.abs(spamAwal - salahSpam) + Math.abs(hamAwal - salahHam);
        buildDisplay.append("\nTingkat Akurasi = "+ ((double)totalAkurat/(totalAkurat + salahHam + salahSpam)));
        //Proses menyusun strng untuk ditampilkan pada file CSV
        pr.appendString("\n;Jumlah spam diketahui;" + spamAwal);
        pr.appendString("\n;Jumlah ham diketahui;" + hamAwal);
        pr.appendString("\n;Jumlah diklasifikasikan spam;" + hasilSpam);
        pr.appendString("\n;Jumlah diklasifikasikan ham;" + hasilHam);
        pr.appendString("\n;Jumlah salah deteksi spam;" + salahSpam);
        pr.appendString("\n;Jumlah salah deteksi ham;" + salahHam);
        pr.appendString("\n;Tingkat Akurasi;"+ ((double)totalAkurat/(totalAkurat + salahHam + salahSpam))+";");
    }
    // menentukan prior
    void set_prior() {
        this.posterior_spam = 1;
        this.posterior_ham = 1;
        this.total_likelihood_spam = 0;
        this.total_likelihood_ham = 0;
        this.evidance = 0;
        this.prior_spam = 0.5;//spam.train.length / (spam.train.length + ham.train.length);
        this.prior_ham = 0.5;//ham.train.length / (spam.train.length + ham.train.length);
    }
    // menentukan posterior, evidance, dan likelihood kelas berdasakan data uji
    public void set_posterior_evidance(int[] data_cari) {
        //perulangan untuk mencari likelihood (perkalian antar likelihood fitur yang ada kata-katanya pada data uji)
        for (int i = 0; i < data_cari.length; i++) {
            if (data_cari[i] != 0) {
                posterior_spam *= spam.likelihood[i];
                posterior_ham *= ham.likelihood[i];
            }
        }
        // nilai likelihood setiap kelasnya
        total_likelihood_spam = posterior_spam;
        total_likelihood_ham = posterior_ham;
        // nilai pembilang setiap kelas (likelihood * prior)
        posterior_spam *= prior_spam;
        posterior_ham *= prior_ham;
        // nilai evidance nya
        evidance = posterior_spam + posterior_ham;
        // nilai posterior setiap kelasnya
        posterior_spam /= evidance;
        posterior_ham /= evidance;

    }
// proses mencetak
    String display(StringBuilder sb) {
        String output = "";
        output += "prior spam : " + prior_spam + " prior ham : " + prior_ham;
        output += "\nlikelihood spam : " + total_likelihood_spam + " \nlikelihood ham : " + total_likelihood_ham;
        output += "\nevidance : " + evidance;
        output += "\nPosterior spam : " + posterior_spam + " \nposterior ham : " + posterior_ham;
        String verdict;
        if (total_likelihood_spam != 1.0 && total_likelihood_ham != 1.0) {
            if (posterior_spam > posterior_ham) {
                verdict = "SPAM";
            } else {
                verdict = "HAM";
            }
        } else {
            verdict = "DATA TAK BISA DITENTUKAN";
        }
        output += "\n" + verdict;

        pr.appendString("\n\n\n");
        pr.appendString(";prior spam;" + String.valueOf(prior_spam) + ";\n");
        pr.appendString(";prior ham;" + String.valueOf(prior_spam) + ";\n");
        pr.appendString(";likelihood spam;" + total_likelihood_spam + ";\n");
        pr.appendString(";likelihood ham;" + total_likelihood_ham + ";\n");
        pr.appendString(";hasilnya;" + verdict + ";\n");
        return output;
    }

//    Proses menyusun tampilan pada textArea ketika ditekan tombol proses multifile
    public void buildDisplay(String kelas) {
        String verdict;
        if (total_likelihood_spam != 1.0 && total_likelihood_ham != 1.0) {
            if (posterior_spam > posterior_ham) {
                verdict = "SPAM";
                hasilSpam += 1;
                salahSpam += kelas.equalsIgnoreCase("ham") ? 1 : 0;
            } else {
                verdict = "HAM";
                hasilHam += 1;
                salahHam += kelas.equalsIgnoreCase("spam") ? 1 : 0;
            }
        } else {
            verdict = "DATA TAK BISA DITENTUKAN";
        }
        tmpMulti.append("\n\n\n");
        tmpMulti.append(";prior spam;" + String.valueOf(prior_spam) + ";\n");
        tmpMulti.append(";prior ham;" + String.valueOf(prior_ham) + ";\n");
        tmpMulti.append(";posterior spam;" + posterior_spam + ";\n");
        tmpMulti.append(";posterior ham;" + posterior_ham + ";\n");
        tmpMulti.append(";likelihood spam;" + total_likelihood_spam + ";\n");
        tmpMulti.append(";likelihood ham;" + total_likelihood_ham + ";\n");
        tmpMulti.append(";kelas awal;" + kelas + ";\n");
        tmpMulti.append(";hasilnya;" + verdict + ";\n");
        
        buildDisplay.append("\nPrior spam = " + String.valueOf(prior_spam) + "\n");
        buildDisplay.append("Prior ham = " + String.valueOf(prior_ham) + "\n");
        buildDisplay.append("Posterior spam = " + posterior_spam + "\n");
        buildDisplay.append("Posterior ham = " + posterior_ham + "\n");
        buildDisplay.append("Likelihood spam = " + total_likelihood_spam + "\n");
        buildDisplay.append("Likelihood ham = " + total_likelihood_ham + "\n");
        buildDisplay.append("Kelas awal = " + kelas + "\n");
        buildDisplay.append("Hasil klasifikasi = " + verdict + "\n");
        
    }
    
    //Proses untuk menampilkan String ke textArea sebagai indikator proses
    public String displayMultiFile(){
        String out = buildDisplay.toString();
        buildDisplay.delete(0, buildDisplay.capacity()-1);
        return out;
    }
}
