/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CobaManual;

/**
 *
 * @author ochim
 */

// kelas fitur digunakan operasi nilai setiapfitur perdata
public class fitur {
    // penamaan kelas dalam setiap fiturnya
    public String kelas;
    // instansiasi object printer untuk hasil cetak 
    Printer pr = new Printer();
    // inisialisasi array beserta fitur textnya
    static String keepAttribute[] = {   "picture",  "make",  "actor",  "lunatic",
                                        "Link",  "meeting",  "amplitude",  "marble",
                                        "money",  "array",  "mentor",
                                        "address",  "offer",  "balance",  "monkey",
                                        "adult",  "order",  "beacon",  "nuclear",
                                        "all",  "original",  "blue",  "outrage",
                                        "bank",  "our",  "bubble",  "peripheral",
                                        "best",  "over",  "chamber",  "potter",
                                        "business",  "paper",  "clerk",  "punch",
                                        "call",  "parts",  "coherent",  "render",
                                        "casino",  "people",  "concert",  "revenue",
                                        "click",  "pm",  "designate",  "spirit",
                                        "conference",  "price",  "disposal",  "steak",
                                        "credit",  "project",  "doubt",  "stunt",
                                        "cs",  "promotion",  "drum",  "tape",
                                        "data",  "quality",  "eagle",  "thread",
                                        "dear",  "re",  "egg",  "tomb",
                                        "direct",  "receive",  "episode",  "tripod",
                                        "edu",  "regards",  "evident",  "twinkle",
                                        "email",  "remove",  "furious",  "upgrade",
                                        "fast",  "report",  "flesh",  "utility",
                                        "font",  "sincerely",  "fault",  "vibration",
                                        "free",  "spam",  "friction",  "violence",
                                        "george",  "table",  "gadget",  "vocal",
                                        "hello",  "take",  "germ",  "vulture",
                                        "here",  "technology",  "gun",  "wedge",
                                        "hi",  "telnet",  "hammer",  "wheel",
                                        "how",  "thank",  "highway",  "wolf",
                                        "hp",  "think",  "humid",  "wrap",
                                        "internet",  "valuable",  "indigo",  "yacht",
                                        "investment",  "we",  "intiment",  "yield",
                                        "lab",  "will",  "interrupt",  "zoo",
                                        "let",  "x",  "kite",  "zip",
                                        "low",  "you",  "lavender",  "women",
                                        "mail",  "your",  "lick",  "weird"
};
    // inisialisasi array untuk pengisian nilai setiap fitur perdatanya
    int[] nilai_fitur = new int[keepAttribute.length];
    // konstruktor jika kelas sudah di tentukan (untuk data training)
    fitur(String kelas){
        this.kelas = kelas;
    }
    // konstruktor jika kelas belum di tentukan (data uji)
    fitur(){}
    // method untuk mengisi nilai setiap fiturnya yang disimpan pada variabel nilai_fitur
    public void isi_nilai(String data){
        // pemecahan data menjadi data email dan nama kelas (nama kelas untuk data uji)
        String verdict[] = data.split("@@class :");
        if((verdict[verdict.length-1] != null || verdict[verdict.length-1] != "") && this.kelas == null){
            this.kelas = verdict[verdict.length-1].replace("\n", "").replace("\r", "");
        }
        // inisialisasi array dan instansiasi textfilter untuk memecah dan menfilter data email menjadi perkata dalam array
        String []output = new TextFilter(verdict[0]).toArray();
        //penambahan nilai_fitur berdasarkan kata(fitur) yang keluar
        for (int i = 0; i < output.length; i++) {
            for (int j = 0; j < keepAttribute.length; j++) {
                if(output[i].equals(keepAttribute[j])){
                    nilai_fitur[j]++;
                }
            }
        }
        for (int i = 0; i < nilai_fitur.length; i++) {
            System.out.printf("| %10d ",nilai_fitur[i]);
            pr.appendString(";" + nilai_fitur[i]);
            
        }System.out.println(kelas);
        pr.appendString(";"+kelas+";\n");
    }
}
