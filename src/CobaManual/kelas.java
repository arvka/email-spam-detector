/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CobaManual;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author ochim
 */
public class kelas {
//    Membuat array sesuai dengan total jumlah fitur
    double [] sum = new double [139];
    //Deklarasi variabel bernama kelas 
    String kelas;
    // Membuat array sesuai dengan total data training per kelas
    fitur[] train = new fitur[50];
    //Membuat array sesuai dengan jumlah fitur untuk menampung nilai likelihood per fitur
    double likelihood[] = new double [sum.length];
    //Instansiasi kelas Printer untuk digunakan penampung nilai String untuk ditampilkan di textbox
    Printer pr = new Printer();
    
    public kelas(String path, String nama_kelas){
        
        kelas = nama_kelas;
        //mengisi nilai dari setiap fitur per data training dengan memanggil method isi nilai pada kelas kelas_spam
        
        for (int i = 0; i < train.length; i++) {
            train[i] = new fitur(kelas);
            System.out.printf("| %7s ","000"+(i+1)+".txt");
            pr.appendString(";000"+(i+1)+".txt");
            train[i].isi_nilai(this.bacaFile(path+"/000"+(i+1)+".txt"));
            
            //menjumlahkan semua nilai setiap fiturnya
            for (int j = 0; j < train[i].nilai_fitur.length; j++) {
                sum[j] += train[i].nilai_fitur[j];
//                System.out.println("data perfitur"+train[i].nilai_fitur[j]);
            }
        } 
        this.likelihood();
    }
    
    //merubah file txt menjadi nilai string
    public String bacaFile(String namaFile) {
		BufferedReader br = null;
		String stringHasil = "";

		try {
			String sCurrentLine;
			br = new BufferedReader(new FileReader(namaFile));
			while ((sCurrentLine = br.readLine()) != null) {
				stringHasil = stringHasil + sCurrentLine + "\n";
			}

		} catch (IOException e) {
			System.out.println("Gagal membaca file " + namaFile);
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return stringHasil;
    }
    
    //mencari likelihood
    public void likelihood(){
        /* mencari likelihood text
        likelihood = ((1 + a) / (b + c))
        a = jumlah nilai setiap fitur pada setiap kelas
        b = penjumlahan dari semua a (penjumlahan semua fitur yang keluar)
        c = banyaknya fitur
        */ 
        double b = 0;
        double c = sum.length;
        for (int i = 0; i < sum.length; i++) {
            b += sum[i];
        }
        for (int i = 0; i < sum.length; i++) {
            double a = sum[i];
            likelihood[i] = (1 + a) / (b + c);
        }
    }
}
