/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CobaManual;

/**
 *
 * @author HP
 */
public class TextFilter {
    String str;
    String[] res;

    //Konstruktor untuk set nilai String yang ingin dilakukan filter
    public TextFilter(String a) {
        this.str = a;
        //Untuk menjalankan fungsi filter
        filtering();
    }
    
    public void filtering(){
        //untuk menjadikan string yang dimasukkan sebagai sekumpulan array
        String[] split = this.str.toLowerCase().split(" ");
        for (int i = 0; i < split.length; i++) {
            split[i] = split[i].replaceAll("[^a-zA-Z]","");
//            Sengaja tidak dimasukkan stemming porter karena pada fitur terdapat kata yang bukan kata dasar
            //split[i] = new PorterStemmer().filter(split[i]);
        }
        this.res = split;
    }
    
    //fungsi untuk mengembalikan nilai yang berbentuk sekumpulan array
    public String[] toArray(){
        return this.res;
    }
    
}
